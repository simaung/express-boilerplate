require('dotenv').config()
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
var promiseMiddleware = require('./src/middleware/promiseMiddleware');
const moment = require('moment');
moment().locale('id')

var v1Router = require('./src/routes/v1/index');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
require('./config/passport')(passport);
app.use(promiseMiddleware());

app.use('/v1', v1Router);

module.exports = app;
