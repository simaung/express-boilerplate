require('dotenv').config();

var prodMode = process.env.PRODUCTION_MODE;
module.exports = {
    'url': (prodMode == 'true') ? process.env.BERSAMAPAY_URL_PROD : process.env.BERSAMAPAY_URL_DEV,
    'secret': (prodMode == 'true') ? process.env.BERSAMAPAY_SECRET_PROD : process.env.BERSAMAPAY_SECRET_DEV,
    'username': (prodMode == 'true') ? process.env.BERSAMAPAY_USERNAME_PROD : process.env.BERSAMAPAY_USERNAME_DEV,
}