const passport = require('passport');
const models = require('../models');
const User = models.User;

module.exports = (req, res, next) => {
    passport.authenticate('jwt', { session: false, }, async (error, token) => {
        if (error || !token) {
            return res.json({status: 401,message: "Unauthorized"})
        }

        try {
            const user = await User.findOne({
                where: { id: token.id },
                attributes: {
                    exclude: ['password']
                }
            });
            req.user = user;
        } catch (error) {
            next(error);
        }
        next();
    })(req, res, next);
}