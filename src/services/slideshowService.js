const redis = require('redis');
const client = redis.createClient();
const models = require('../models');
const Slideshow = models.Slideshow;
const commonUtils = require('../utils').Common;
const { validationResult } = require('express-validator');

const getAll = async (req) => {

    const page = parseInt(req.query.page) || 1;
    const perPage = parseInt(req.query.perPage) || 10;

    let where = {};

    if (req.query.status)
        where.status = req.query.status

    if (req.query.type)
        where.type = req.query.type

    return await Slideshow.findAndCountAll({
        limit: perPage,
        offset: ((page - 1) * perPage),
        where: where
    })
        .then((slideshows) => {
            let data = commonUtils.pagination(slideshows, page, perPage);

            return { status: 200, message: 'success', result: data }
        })
        .catch(error => {
            throw Error(error);
        })
}

const getOne = async (id) => {

    await client.connect().catch(error => { })
    const redisKey = 'slideshow-' + id;

    return await client.get(redisKey)
        .then(cacheData => {
            if (cacheData) {
                return { status: 200, message: 'success', result: JSON.parse(cacheData), fromCache: true };
            } else {
                return Slideshow.findByPk(id)
                    .then(slideshow => {
                        if (!slideshow) return { status: 404, message: 'Not Found' }
                        client.set(redisKey, JSON.stringify(slideshow));
                        client.expire(redisKey, 60 * 60);
                        return { status: 200, message: 'success', result: slideshow, fromCache: false }
                    })
                    .catch(error => {
                        throw Error(error);
                    })
            }
        })
        .catch(error => {
            throw Error(error);
        });
}

const createOne = async (req) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return { status: 422, message: errors.array() };
    }
    return await Slideshow.create(req.body)
        .then((slideshow) => {
            return { status: 201, message: 'data created', result: slideshow }
        })
        .catch(error => {
            throw Error(error);
        })
}

const deleteOne = async (id) => {
    return Slideshow.findByPk(id)
        .then(slideshow => {
            if (!slideshow) return { status: 404, message: 'Not Found' }
            slideshow.destroy();
            return { status: 200, message: 'success deleted', result: slideshow }
        })
        .catch(error => {
            throw Error(error);
        })
}

module.exports = {
    getAll, getOne, createOne, deleteOne
}
