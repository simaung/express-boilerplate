const { validationResult } = require('express-validator');
const moment = require('moment');
const { secret, username, url } = require('../../config/bersamapay');
const CryptoJS = require('crypto-js');
const axios = require('axios');

const timestampHeader = (new Date()).toString();
const timestamp = () => {
    return moment().format("DDMMYYHHmmss");
}
// console.log(timestamp(), 'timestamp service');

const _authorizationHeader = (uri) => {
    const client_id_hmac = CryptoJS.HmacSHA256(username, secret);
    var client_id_hash = CryptoJS.enc.Base64.stringify(client_id_hmac);
    client_id_hash = client_id_hash.replace(/\+/gi, '-').replace(/\//gi, '_').replace(/\=/gi, '');

    var sha256digest = CryptoJS.SHA256(data);
    var sha256 = CryptoJS.enc.Hex.stringify(sha256digest);

    var wordArray = CryptoJS.enc.Utf8.parse(sha256);

    var content_digest = CryptoJS.enc.Base64.stringify(wordArray);
    content_digest = content_digest.replace(/\+/gi, '-').replace(/\//gi, '_').replace(/\=/gi, '');

    var string_to_sign = "POST" + "\n" + timestampHeader + "\n" + uri + "\n" + content_digest;

    var hmac_signature = CryptoJS.HmacSHA256(string_to_sign, secret);
    var base64_encoded_hmac_signature = CryptoJS.enc.Base64.stringify(hmac_signature);
    base64_encoded_hmac_signature = base64_encoded_hmac_signature.replace(/\+/gi, '-').replace(/\//gi, '_').replace(/\=/gi, '');

    const authorizationHeader = client_id_hash + ":" + base64_encoded_hmac_signature;

    return authorizationHeader;
}

const sellerRegistration = async (req) => {
    const uri = '/rest/api/sellerRegistration';
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return { status: 422, message: errors.array() };
    }

    data = {
        "sellerRegistrationRequest": {
            "apiVersion": "1.0",
            "timestamp": timestamp(),
            "merchantID": req.body.merchantID,
            "registrationID": req.body.registrationID,
            "sellerName": req.body.sellerName,
            "sellerAddress": req.body.sellerAddress,
            "bankCode": req.body.bankCode,
            "benefAccNumber": req.body.benefAccNumber,
            "benefName": req.body.benefName,
            "regencyCode": req.body.regencyCode
        }
    }

    data = JSON.stringify(data)

    var postBody = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(data));

    var response = await axios.post(url + uri,
        postBody
        , {
            headers: {
                'Content-Type': 'application/json',
                'Date': timestampHeader,
                'Authorization': _authorizationHeader(uri),
                'username': username
            }
        })
        .then(function (response) {
            var parsedWord = CryptoJS.enc.Base64.parse(response.data)
            var parsedStr = parsedWord.toString(CryptoJS.enc.Utf8)

            return { status: 200, message: 'success', result: JSON.parse(parsedStr) }
        })
        .catch(function (error) {
            return { status: error.response.status, message: 'false', result: error.response.data }
        })

    return response;
}

const checkSellerRegistration = async (req) => {
    const uri = '/rest/api/checkStatusSellerRegistration';
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return { status: 422, message: errors.array() };
    }

    data = {
        "checkStatusSellerRegistration": {
            "apiVersion": "1.0",
            "timestamp": timestamp(),
            "merchantID": req.body.merchantID,
            "registrationID": req.body.registrationID,
        }
    }

    data = JSON.stringify(data)

    var postBody = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(data));

    var response = await axios.post(url + uri,
        postBody
        , {
            headers: {
                'Content-Type': 'application/json',
                'Date': timestampHeader,
                'Authorization': _authorizationHeader(uri),
                'username': username
            }
        })
        .then(function (response) {
            var parsedWord = CryptoJS.enc.Base64.parse(response.data)
            var parsedStr = parsedWord.toString(CryptoJS.enc.Utf8)

            return { status: 200, message: 'success', result: JSON.parse(parsedStr) }
        })
        .catch(function (error) {
            return { status: error.response.status, message: 'false', result: error.response.data }
        })

    return response;
}

const getPurchase = async (req) => {
    const uri = '/rest/api/getPurchase';
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return { status: 422, message: errors.array() };
    }

    data = {
        "PaymentRequest": {
            "version": "8.0",
            "timestamp": timestamp(),
            "merchantID": req.body.merchantID,
            "uniqueTransactionCode": req.body.uniqueTransactionCode,
            "currencyCode": '360',
            "customerIdentifier": req.body.customerIdentifier,
            "serviceFee": req.body.serviceFee,
            "origingoodsPrice": req.body.purchaseProductPrice,
            "totalAmount": req.body.totalAmount,
            "merchantRedirectURL": req.body.merchantRedirectUrl,
            "userDefined1": req.body.userDefined1,
            "userDefined2": req.body.userDefined2,
            "msisdn": req.body.customerIdentifier,
            "trxType": 'paymentonly',
            // "shippingcostAmount": req.body.shippingcostAmount,
            // "discountRule": req.body.discountRule,
            // "discountAmount": req.body.discountAmount,
            // "origingoodsPrice": req.body.origingoodsPrice,
        }
    }

    dataOri = JSON.stringify(data);

    data = JSON.stringify(data)

    var postBody = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(data));

    var response = await axios.post(url + uri,
        postBody
        , {
            headers: {
                'Content-Type': 'application/json',
                'Date': timestampHeader,
                'Authorization': _authorizationHeader(uri),
                'username': username
            }
        })
        .then(function (response) {
            var parsedWord = CryptoJS.enc.Base64.parse(response.data)
            var parsedStr = parsedWord.toString(CryptoJS.enc.Utf8)

            return { status: 200, message: 'success', result: JSON.parse(parsedStr), request: dataOri }
        })
        .catch(function (error) {
            return { status: error.response.status, message: 'false', result: error.response.data }
        })

    return response;
}

const checkStatus = async (req) => {
    const uri = '/rest/api/sof_status';
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return { status: 422, message: errors.array() };
    }

    data = {
        "StatusRequest": {
            "uniqueTransactionCode": req.body.uniqueTransactionCode
        }
    }

    data = JSON.stringify(data)

    var postBody = CryptoJS.enc.Base64.stringify(CryptoJS.enc.Utf8.parse(data));

    var response = await axios.post(url + uri,
        postBody
        , {
            headers: {
                'Content-Type': 'application/json',
                'Date': timestampHeader,
                'Authorization': _authorizationHeader(uri),
                'username': username
            }
        })
        .then(function (response) {
            var parsedWord = CryptoJS.enc.Base64.parse(response.data)
            var parsedStr = parsedWord.toString(CryptoJS.enc.Utf8)

            return { status: 200, message: 'success', result: JSON.parse(parsedStr) }
        })
        .catch(function (error) {
            return { status: error.response.status, message: 'false', result: error.response.data }
        })

    return response;

}

const base64Decode = async (req) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return { status: 422, message: errors.array() };
    }
    var parsedWord = CryptoJS.enc.Base64.parse(req.body.code)
    var parsedStr = parsedWord.toString(CryptoJS.enc.Utf8)

    return { status: 200, message: 'success', result: JSON.parse(parsedStr) }
}

module.exports = {
    sellerRegistration, checkSellerRegistration, getPurchase, checkStatus, base64Decode
}
