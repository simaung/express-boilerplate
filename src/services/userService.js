const jwt = require('jsonwebtoken');
const models = require('../models');
const User = models.User;
const { validationResult } = require('express-validator');

const register = async (req) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return { status: 422, message: errors.array() };
    }

    return await User.create(req.body)
        .then(() => {
            return { status: 201, msg: "Register successfully", };
        })
        .catch(error => {
            throw Error(error);
        })
}

const login = async (req) => {
    return await User.findOne({
        where: {
            email: req.body.email
        }
    })
        .then(async (user) => {
            if (!user) return { status: 404, message: 'email not exist' };

            if (!user.password || !await user.validPassword(req.body.password, user.password)) {
                return { status: 400, message: 'password invalid', };
            } else {
                var payload = { id: user.id }
                const token = jwt.sign(payload, process.env.JWT_SECRET);

                return { status: 200, message: "login successfully", token: "Bearer " + token };
            };
        })
        .catch(error => {
            throw Error(error);
        })
}

const getUser = async (req) => {
    return { status: 200, message: 'success', result: req };
}

module.exports = {
    register, login, getUser
}
