'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Slideshow extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Slideshow.init({
    title: DataTypes.STRING,
    desc: DataTypes.STRING,
    url: DataTypes.STRING,
    image: DataTypes.STRING,
    status: DataTypes.ENUM('active','inactive'),
    type: DataTypes.ENUM('admin','member','guest','all'),
    order: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Slideshow',
  });
  return Slideshow;
};