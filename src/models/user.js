'use strict';
const bcrypt = require('bcrypt');
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    phone: DataTypes.STRING,
  },
    {
      hooks: {
        beforeCreate: async (User) => {
          if (User.password) {
            const salt = await bcrypt.genSaltSync(10);
            User.password = bcrypt.hashSync(User.password, salt);
          }
        },
        beforeUpdate: async (User) => {
          if (User.password) {
            const salt = await bcrypt.genSaltSync(10);
            User.password = bcrypt.hashSync(User.password, salt);
          }
        }
      },
      instanceMethods: {
        validPassword: (password) => {
          return bcrypt.compareSync(password, this.password);
        }
      },
      sequelize,
      modelName: 'User',
    });
  User.prototype.validPassword = async (password, hash) => {
    return await bcrypt.compareSync(password, hash);
  }
  return User;
};