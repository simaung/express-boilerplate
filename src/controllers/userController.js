const userService = require('../services/userService');
const models = require('../models');
const User = models.User;
const { body } = require('express-validator');

const register = (req, res) => {
    res.promise(async () => await userService.register(req));
}

const login = (req, res) => {
    res.promise(async () => await userService.login(req));
}

const getUser = (req, res) => {
    res.promise(async () => await userService.getUser(req.user));
}

const validate = (method) => {
    switch (method) {
        case 'register': {
            return [
                body('email')
                    .not().isEmpty()
                    .isEmail()
                    .withMessage('Email field is required')
                    .custom(async (value, { req }) => {
                        return await User.findOne({
                            where: {
                                email: req.body.email
                            }
                        })
                            .then(user => {
                                if (user) return Promise.reject('Email already exists');
                                return true;
                            })
                            .catch(error => { throw error })
                    }),
                body('password')
                    .trim()
                    .isLength({ min: 8 })
                    .withMessage('Password minimum 8 character'),
                body('confirm_password')
                    .trim()
                    .custom((value, { req }) => {
                        if (value != req.body.password) {
                            return Promise.reject('Password mistach')
                        }
                        return true;
                    })

            ]
        }
    }
}

module.exports = {
    register, login, getUser, validate
}
