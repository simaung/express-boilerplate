const slideshowService = require('../services/slideshowService');
const { body } = require('express-validator');

const getAll = async (req, res) => {
    res.promise(async () => await slideshowService.getAll(req));
}

const getOne = (req, res) => {
    res.promise(async () => await slideshowService.getOne(req.params.id));

    /* diganti sama res.promise article from https://www.toptal.com/express-js/routes-js-promises-error-handling
    return await slideshowService.getOne(req.params.id)
        .then(result => {
            res.status(result.status).send(result);
        })
        .catch(error => {
            res.send({ status: 500, message: error.message });
        })
    */
}

const createOne = (req, res) => {
    res.promise(async () => await slideshowService.createOne(req));
}

const deleteOne = (req, res) => {
    res.promise(async () => await slideshowService.deleteOne(req.params.id));
}

const validate = (method) => {
    switch (method) {
        case 'create': {
            return [
                body('title')
                    .not().isEmpty()
                    .withMessage('Title field is required'),
                body('desc')
                    .not().isEmpty()
                    .withMessage('Desc field is required')
            ]
        }
    }
}

module.exports = {
    getAll, getOne, createOne, deleteOne, validate
}
