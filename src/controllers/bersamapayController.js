const bersamapayService = require('../services/bersamapayService');
const { body } = require('express-validator');

const sellerRegistration = async (req, res) => {
    res.promise(async () => await bersamapayService.sellerRegistration(req));
}

const checkSellerRegistration = async (req, res) => {
    res.promise(async () => await bersamapayService.checkSellerRegistration(req));
}

const getPurchase = async (req, res) => {
    res.promise(async () => await bersamapayService.getPurchase(req));
}

const checkStatus = async (req, res) => {
    res.promise(async () => await bersamapayService.checkStatus(req));
}

const base64Decode = async (req, res) => {
    res.promise(async () => await bersamapayService.base64Decode(req));
}

const validate = (method) => {
    switch (method) {
        case 'create': {
            return [
                body('sellerName')
                    .not().isEmpty()
                    .withMessage('sellerName field is required'),
                body('sellerAddress')
                    .not().isEmpty()
                    .withMessage('sellerAddress field is required'),
                body('benefAccNumber')
                    .not().isEmpty()
                    .withMessage('benefAccNumber field is required'),
                body('benefName')
                    .not().isEmpty()
                    .withMessage('benefName field is required'),
                body('bankCode')
                    .not().isEmpty()
                    .withMessage('bankCode field is required')
            ]
        }
        case 'checkRegister': {
            return [
                body('merchantID')
                    .not().isEmpty()
                    .withMessage('merchantID field is required'),
                body('registrationID')
                    .not().isEmpty()
                    .withMessage('registrationID field is required'),
            ]
        }
        case 'getPurchase': {
            return [
                body('merchantID')
                    .not().isEmpty()
                    .withMessage('merchantID field is required'),
                body('uniqueTransactionCode')
                    .not().isEmpty()
                    .withMessage('uniqueTransactionCode field is required')
                    .isLength({ max: 12 })
                    .withMessage('uniqueTransactionCode field maximum 12 length'),
                body('customerIdentifier')
                    .not().isEmpty()
                    .withMessage('customerIdentifier field is required'),
                body('serviceFee')
                    .not().isEmpty()
                    .withMessage('serviceFee field is required'),
                body('purchaseProductPrice')
                    .not().isEmpty()
                    .withMessage('purchaseProductPrice field is required'),
                body('totalAmount')
                    .not().isEmpty()
                    .withMessage('totalAmount field is required'),
                body('merchantRedirectUrl')
                    .not().isEmpty()
                    .withMessage('merchantRedirectUrl field is required'),
                body('userDefined1')
                    .not().isEmpty()
                    .withMessage('userDefined1 field is required'),
            ]
        }
        case 'checkStatus': {
            return [
                body('uniqueTransactionCode')
                    .not().isEmpty()
                    .withMessage('uniqueTransactionCode field is required')
            ]
        }
        case 'base64Decode': {
            return [
                body('code')
                    .not().isEmpty()
                    .withMessage('code field is required')
            ]
        }
    }
}

module.exports = {
    sellerRegistration, checkSellerRegistration, getPurchase, checkStatus, base64Decode, validate
}
