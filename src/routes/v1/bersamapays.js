var express = require('express');
var router = express.Router();
const bersamapayController = require('../../controllers/bersamapayController');

router.post('/sellerRegistration', bersamapayController.validate('create'), bersamapayController.sellerRegistration);
router.post('/checkSellerRegistration', bersamapayController.validate('checkRegister'), bersamapayController.checkSellerRegistration);
router.post('/getPurchase', bersamapayController.validate('getPurchase'), bersamapayController.getPurchase);
router.post('/checkStatus', bersamapayController.validate('checkStatus'), bersamapayController.checkStatus);
router.post('/base64Decode', bersamapayController.validate('base64Decode'), bersamapayController.base64Decode);

module.exports = router;
