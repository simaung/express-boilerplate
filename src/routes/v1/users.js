var express = require('express');
// const passport = require('passport');
var router = express.Router();
const userController = require('./../../controllers/userController');
const authenticateMiddleware = require('../../middleware/authenticateMiddelware');

router.post('/register', userController.validate('register'), userController.register);
router.post('/login', userController.login);
router.get('/', authenticateMiddleware, userController.getUser);
// router.get('/', passport.authenticate("jwt", { session: false }), userController.getUser);

module.exports = router;
