var express = require('express');
var router = express.Router();

var usersRouter = require('./users');
var slideshowsRouter = require('./slideshows');
var bersamapaysRouter = require('./bersamapays');

router.use('/users', usersRouter);
router.use('/slideshows', slideshowsRouter);
router.use('/bersamapays', bersamapaysRouter);

module.exports = router;
