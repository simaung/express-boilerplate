var express = require('express');
var router = express.Router();
const slideshowController = require('./../../controllers/slideshowController');

router.get('/', slideshowController.getAll);
router.get('/:id', slideshowController.getOne);
router.post('/', slideshowController.validate('create'), slideshowController.createOne);
router.delete('/:id', slideshowController.deleteOne);

module.exports = router;
