module.exports = {
    pagination(data, page, perPage) {
        let paginate = {
            'total': data.count,
            'current_page': page,
            'per_page': perPage,
            'total_page': Math.ceil(data.count / perPage),
        };

        return {
            data: data.rows,
            pagination: paginate
        }
    }
}

