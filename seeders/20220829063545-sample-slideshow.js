'use strict';

var faker = require('faker')
const { v4: uuidv4 } = require('uuid');

var slideshows = [...Array(30)].map(() => (
  {
    id: uuidv4(),
    title: faker.lorem.word(),
    desc: faker.lorem.word(),
    image: faker.image.imageUrl(),
    createdAt: new Date(),
    updatedAt: new Date()
  }
));

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    return queryInterface.bulkInsert('Slideshows', slideshows, {})
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

    return queryInterface.bulkDelete('Slideshows', null, {});
  }
};
